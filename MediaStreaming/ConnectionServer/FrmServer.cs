﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConnectionServer
{
    public partial class FrmServer : Form
    {
        // private properties
        private bool isRunning = false;

        // Private functions.
        private void log(string message)
        {
            Invoke(new Action(() => {
                txtLog.Text += message + "\r\n";
            }));
        }
        private void quit()
        {
            // terminate all child process.

            // close window
            Application.ExitThread();
        }

        private void connect()
        {

        }
        private void disconnect()
        {

        }
        // Event Call backs...
        public FrmServer()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            log("Streaming Server started.");
        }
        // Quit button click
        private void mnuQuit_Click(object sender, EventArgs e)
        {
            if(DialogResult.OK == MessageBox.Show("Are you sure to close server?","Attension", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
            {
                quit();
            }
        }
        //Connect button click
        private void mnuConnect_Click(object sender, EventArgs e)
        {
            if (isRunning)
                disconnect();
            else
                connect();
        }

        private void FrmServer_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Visible = true;
        }

    }
}
